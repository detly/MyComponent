import QtQuick 2.10
import QtQuick.Window 2.10

Window {
    visible: true
    width: 800
    height: 250
    title: qsTr("Hello World")

    MyComponent {
        range: 10
        value: 0
    }

}
