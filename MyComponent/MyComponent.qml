import QtQuick 2.0
import QtQuick.Controls 2.2

Control {
    id: dut

    implicitWidth: 200
    implicitHeight: 50

    property int range
    property int value

    onValueChanged: {
        console.log("New value: " + value);
    }

    Rectangle {
        width: dut.width
        height: dut.height
        color: Qt.rgba(0,0,0,0)
        border.color: Qt.rgba(0,0,0,1)
        border.width: 1
    }

    Rectangle {
        width: dut.width * dut.value/dut.range
        height: dut.height
        color: Qt.rgba(0,0,0,1)
    }

    MouseArea {
        id: mouseArea
        anchors.fill: dut
    }

    Binding {
        target: dut
        property: 'value'
        value: Math.round(mouseArea.mouseX/mouseArea.width * dut.range);
        when: mouseArea.pressed && mouseArea.containsMouse
    }
}
